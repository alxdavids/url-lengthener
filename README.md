# URL Lengthener

Reads in Twitter API data from CSV and returns the CSV with URLs
expanded.

## quickstart

```bash
yarn install
export DATA_DIR=<full_path_to_data_dir>
export NODE_OPTIONS="--max-old-space-size=4096"
yarn expand
```

To run as a test (only for the directory `0104`) run the following
before running the above instructions.

```bash
export TEST=1
```

If you would like to overwrite previously existing outputs, run:

```bash
export OVERWRITE=1
```

### run locally

```bash
export DATA_DIR=<full_path_to_data_dir>
export NODE_OPTIONS="--max-old-space-size=4096"
make init
make expand
```
