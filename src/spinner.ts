import { default as ora } from "ora";

// progress spinner settings
const ORA_COLORS: Array<
  | "magenta"
  | "black"
  | "red"
  | "green"
  | "yellow"
  | "blue"
  | "cyan"
  | "white"
  | "gray"
> = [
  "magenta",
  // "black", removed as it doesn't show well
  "red",
  "green",
  "yellow",
  "blue",
  "cyan",
  "white",
  "gray",
];
const randomInt = (max: number) =>
  Math.floor(Math.random() * Math.floor(max));
export class Spinner {
  spinner: ora.Ora;

  constructor(text: string) {
    this.spinner = ora({
      text: text,
      spinner: "material",
      color: ORA_COLORS[randomInt(ORA_COLORS.length)],
    });
  }

  start(): void {
    this.spinner.start();
  }

  stop(): void {
    this.spinner.stop();
  }

  succeed(text: string): void {
    this.spinner.succeed(text);
  }

  fail(text: string): void {
    this.spinner.fail(text);
  }
}
