import * as fs from "fs";
import * as path from "path";
import * as csv from "fast-csv";
import fetch from "node-fetch";
import { Spinner } from "./spinner";
import cp from "child_process";
import * as yargs from "yargs";
import { default as del } from "del";

// All tiny URLs to consider
class TinyURL {
  prefix: string;
  pathLength: number;
  urlLength: number;

  constructor(prefix: string, pathLength: number) {
    this.prefix = prefix;
    this.pathLength = pathLength;
    this.urlLength = prefix.length + pathLength;
  }
}

const argv = yargs
  .option("retry", {
    description: "Retry writing error cases",
    alias: "r",
    type: "boolean",
  })
  .option("combine", {
    description: "Combine files from each directory",
    alias: "c",
    type: "boolean",
  })
  .option("data", {
    description: "Optional flag for specifying data directory",
    alias: "d",
    type: "string",
  })
  .option("overwrite", {
    description: "Specifies whether to overwrite existing files",
    alias: "o",
    type: "boolean",
  })
  .option("test", {
    description:
      "Specifies whether the program is being run in test mode or not",
    alias: "t",
    type: "boolean",
  })
  .help()
  .alias("help", "h").argv;

// utility functions for getting dirs and files
const getDir = (dirName: string) =>
  path.resolve(__dirname, "../", dirName);
const DATA_DIR = argv.data || process.env["DATA_DIR"];
if (!DATA_DIR) {
  throw new Error(
    "DATA_DIR must be set. Run 'export DATA_DIR=<path_to_data_directory>'",
  );
}
const OUTPUT_DIR = getDir("output");
const BACKUP_DIR = getDir("backup");
const getInnerDataDir = (dirName: string) =>
  path.resolve(DATA_DIR, dirName);
const getDataFile = (dirName: string, fileName: string) =>
  path.resolve(DATA_DIR, dirName, fileName);
const getInnerOutputDir = (dirName: string) =>
  path.resolve(OUTPUT_DIR, dirName);
const getOutputFile = (dirName: string, fileName: string) =>
  path.resolve(OUTPUT_DIR, dirName, fileName);
const outputDirExists = (dirName: string) =>
  fs.existsSync(getInnerOutputDir(dirName));
const outputFileExists = (dirName: string, fileName: string) =>
  fs.existsSync(getOutputFile(dirName, fileName));

/**
 * Dictates the functionality required for a fetch response
 */
interface FetchResponse {
  tinyURL: string;

  toArray(): Array<string>;
}

/**
 * Denotes a successful fetch response
 */
class FetchSuccess implements FetchResponse {
  tinyURL: string;
  fullURL: string;

  constructor(tinyURL: string, fullURL: string) {
    this.tinyURL = tinyURL;
    this.fullURL = fullURL;
  }

  toArray() {
    return [this.tinyURL, this.fullURL];
  }
}

/**
 * Denotes a fetch request that terminated in an HTTP error
 */
class FetchError implements FetchResponse {
  tinyURL: string;

  constructor(tinyURL: string) {
    this.tinyURL = tinyURL;
  }

  toArray() {
    return [this.tinyURL];
  }
}

/**
 * Returns a new FetchResponse promise to expand URL
 * @param urlString
 */
async function createFetch(urlString: string): Promise<FetchResponse> {
  const pFetch = fetch(urlString, {
    redirect: "manual",
  })
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .then((resp: any) => {
      if (resp.status !== 301 && resp.status !== 302) {
        console.error(
          `\nBad status code received from tiny URL, url: ${urlString}, statusCode: ${resp.status}`,
        );
        return new FetchError(urlString);
      }

      // get contents of location header
      const location: string = resp.headers.get("location");
      if (!location) {
        console.error(
          `\nLocation header not specified, url: ${urlString}`,
        );
        return new FetchError(urlString);
      }

      // push to Output rows
      return new FetchSuccess(urlString, location);
    })
    .catch((err: Error) => {
      console.error(
        `\nAn unexpected error occurred fetching: ${urlString}, error: ${err}`,
      );
      return new FetchError(urlString);
    });
  return pFetch;
}

/**
 * Creates rows from the fetch data
 */
function createRows(
  fetchResponses: Array<FetchResponse>,
  urlToTweetIDs: Record<string, Array<string>>,
): Array<Array<string>> {
  const rowsForEachURL = fetchResponses.map((resp) => {
    const tinyURL = resp.tinyURL;
    const tweetIDs = urlToTweetIDs[tinyURL];
    if (!tweetIDs) {
      throw new Error(
        `An error occurred: no tweet IDs were found for ${tinyURL}`,
      );
    }

    return tweetIDs.map((id) =>
      resp instanceof FetchSuccess
        ? [id, tinyURL, resp.fullURL]
        : [id, tinyURL],
    );
  });

  // join arrays
  return rowsForEachURL.reduce((r, e) => r.concat(e), [[]]);
}

if (!argv.retry && !argv.combine) {
  // CSV input structure
  const TWEET_ID_INDEX = 0;
  const TWEET_COLUMN_INDEX = 4;
  const TWEET_TINY_URLS = [new TinyURL("https://t.co/", 10)];

  // Disallowed directories due to bad data formats
  const DISALLOWED_DIRECTORIES = ["1703", "1803"];

  const isTest = () => {
    const test = process.env["TEST"];
    return Boolean((test && test === "1") || argv.test);
  };
  console.log(`Running in test mode: ${isTest()}`);
  const shouldOverwrite = () => {
    const ow = process.env["OVERWRITE"];
    return Boolean((ow && ow === "1") || argv.overwrite);
  };
  console.log(`Will overwrite existing output: ${shouldOverwrite()}`);
  if (!shouldOverwrite()) {
    console.log(
      "If you would like to overwrite old output files, run 'export OVERWRITE=1' and re-run the program.",
    );
  }

  /**
   * Compares the lines in both files using the bash command wc
   * @param backupPath
   * @param originalFilePath
   */
  async function compareFileLines(
    backupPath: string,
    originalFilePath: string,
  ): Promise<boolean> {
    return new Promise((resolve) => {
      cp.exec(
        `wc -l < ${backupPath}`,
        function (errorBackup, wcBackup) {
          if (errorBackup) {
            console.error(
              `Error occurred, keeping backup at ${backupPath}, error: ${errorBackup}`,
            );
            resolve(false);
          }
          cp.exec(
            `wc -l < ${originalFilePath}`,
            function (errorOriginal, wcOriginal) {
              if (errorOriginal) {
                console.error(
                  `Error occurred reading original file, keeping backup at ${backupPath}, error: ${errorOriginal}`,
                );
                resolve(false);
              }

              if (wcBackup !== wcOriginal) {
                console.error(
                  `Line count in original (${wcOriginal}) does not match backup (${wcBackup}). Keeping backup file.`,
                );
                resolve(false);
              }

              resolve(true);
            },
          );
        },
      );
    });
  }

  /**
   * Read in CSV file contents and output a CSV file with rows of the
   * form:
   * "tweetID","tinyURL","fullURL"
   * @param fileName
   */
  async function urlExpander(dataDir: string, fileName: string) {
    const fetches: Array<Promise<FetchResponse>> = [];
    const urlToTweetIDs: Record<string, Array<string>> = {};
    let fetchOutput: Array<FetchResponse> = [];

    const pRead: Promise<void> = new Promise((resolve) => {
      const spinnerFetches = new Spinner(
        `Accumulating fetch calls for ${fileName}`,
      );
      spinnerFetches.start();
      fs.createReadStream(getDataFile(dataDir, fileName))
        .pipe(csv.parse({ headers: false }))
        .on("error", (error: Error) => console.error(error))
        .on("data", (row: Array<string>) => {
          const tweetID = row[TWEET_ID_INDEX];
          const tweet = row[TWEET_COLUMN_INDEX];

          // loop throughs to consider all prefixes to expand
          TWEET_TINY_URLS.forEach((tinyURL) => {
            const urlIndex = tweet.indexOf(tinyURL.prefix);
            // utility functions for getting dirs and filesexOf(tinyURL.prefix);
            if (urlIndex === -1) {
              return;
            }
            const urlString = tweet.substring(
              urlIndex,
              urlIndex + tinyURL.urlLength,
            );

            if (!urlToTweetIDs[urlString]) {
              // Create mapping from URL string to tweet IDs
              urlToTweetIDs[urlString] = [tweetID];
            } else {
              // if mapping already created then we already have a fetch up
              urlToTweetIDs[urlString].push(tweetID);
              return;
            }

            // create HTTP request promise for running in async
            const pFetch = createFetch(urlString);
            // add promise to be checked asynchronously
            fetches.push(pFetch);
          });
        })
        .on("end", async (rowCount: number) => {
          spinnerFetches.succeed(
            `${fetches.length} fetch calls accumulated for ${fileName} from ${rowCount} rows.`,
          );
          if (fetches.length === 0) {
            console.log(`No fetches to make for ${fileName}`);
            resolve();
          }

          // wait for HTTP requests to complete
          const spinner = new Spinner(
            `Expanding URLs for ${fileName} ...`,
          );
          spinner.start();
          fetchOutput = await Promise.all(fetches);
          spinner.succeed("Fetching complete");

          resolve();
        });
    });

    // wait for file promise to conclude
    await pRead;

    const spinnerOutput = new Spinner(
      `Writing output rows for ${fileName}`,
    );
    spinnerOutput.start();
    // delete and reopen a new file, might as well do sync here
    if (!fs.existsSync(OUTPUT_DIR)) {
      fs.mkdirSync(OUTPUT_DIR);
    }
    // check if the inner file structure exists yet
    if (!outputDirExists(dataDir)) {
      fs.mkdirSync(getInnerOutputDir(dataDir));
    } else if (outputFileExists(dataDir, fileName)) {
      fs.unlinkSync(getOutputFile(dataDir, fileName));
    }
    fs.openSync(getOutputFile(dataDir, fileName), "w");

    // delete and reopen a new file, might as well do sync here
    if (!fs.existsSync(OUTPUT_DIR)) {
      fs.mkdirSync(OUTPUT_DIR);
    } else if (fs.existsSync(getOutputFile(dataDir, fileName))) {
      fs.unlinkSync(getOutputFile(dataDir, fileName));
    }

    // write to file
    const successfulOutput = fetchOutput.filter(
      (row) => row instanceof FetchSuccess,
    );
    const rowsSuccess = createRows(successfulOutput, urlToTweetIDs);
    csv
      .writeToPath(getOutputFile(dataDir, fileName), rowsSuccess)
      .on("error", (err) => console.error(err))
      .on("finish", () =>
        spinnerOutput.succeed(
          `Done writing ${fileName} with ${rowsSuccess.length} lines.`,
        ),
      );
    const errorOutput = fetchOutput.filter(
      (row) => row instanceof FetchError,
    );
    if (errorOutput.length > 0) {
      const spinnerErrors = new Spinner(
        `Writing error rows for ${fileName}`,
      );
      spinnerErrors.start();
      const rowsError = createRows(errorOutput, urlToTweetIDs);
      const errorsDir = path.resolve(OUTPUT_DIR, "errors");
      const errorsInnerDir = path.resolve(errorsDir, dataDir);
      if (!fs.existsSync(errorsDir)) {
        fs.mkdirSync(errorsDir);
      }
      if (!fs.existsSync(errorsInnerDir)) {
        fs.mkdirSync(errorsInnerDir);
      }
      const errorFileName = fileName.replace(".csv", "_errors.csv");
      const errorsFilePath = path.resolve(
        errorsInnerDir,
        errorFileName,
      );
      fs.openSync(errorsFilePath, "w");
      csv
        .writeToPath(errorsFilePath, rowsError)
        .on("error", (err) => console.error(err))
        .on("finish", () =>
          spinnerErrors.succeed(
            `Done writing ${errorFileName} with ${rowsError.length} lines.`,
          ),
        );
    }
  }

  // read in all subdirectories
  const dirNames = fs.readdirSync(DATA_DIR);
  console.log(
    `Directories found: ${JSON.stringify(dirNames, null, 4)}`,
  );
  const dirLoop = async (dNames: Array<string>) => {
    for (let i = 0; i < dNames.length; i++) {
      const dName = dNames[i];
      // check if the file is a directory
      const stats = fs.lstatSync(path.resolve(DATA_DIR, dName));
      if (!stats.isDirectory()) {
        console.log(`${dName} is not a directory, skipping.`);
        continue;
      }
      // only run for "0104 as a test for now"
      if (isTest() && dName !== "0104") {
        console.log("running in test mode");
        console.log(`skipping ${dName}...`);
        continue;
      } else if (DISALLOWED_DIRECTORIES.includes(dName)) {
        console.log(`${dName} is disallowed, ignoring.`);
        continue;
      }

      // create local backup dir
      const backupDir = path.resolve(BACKUP_DIR, dName);
      if (!fs.existsSync(BACKUP_DIR)) {
        fs.mkdirSync(BACKUP_DIR);
      }
      if (!fs.existsSync(backupDir)) {
        fs.mkdirSync(backupDir);
      }

      // Create loops for each individual file
      const innerLoop = async (fileNames: Array<string>) => {
        for (let i = 0; i < fileNames.length; i++) {
          // backup file
          const fName = fileNames[i];
          if (!shouldOverwrite() && outputFileExists(dName, fName)) {
            console.log(`${fName} exists, not overwriting.`);
            continue;
          }
          const originalFilePath = getDataFile(dName, fName);
          const backupPath = path.resolve(backupDir, fName);
          const spinnerBackup = new Spinner(`Backing up ${fName}`);
          spinnerBackup.start();
          fs.copyFileSync(originalFilePath, backupPath);
          spinnerBackup.succeed(`${fName} backed up to ${backupPath}`);

          // expand URLs
          await urlExpander(dName, fileNames[i]);

          const spinnerRestore = new Spinner(
            `Remove backup for ${fName}`,
          );
          spinnerRestore.start();
          const identical = await compareFileLines(
            backupPath,
            originalFilePath,
          );
          if (identical) {
            fs.unlinkSync(backupPath);
            spinnerRestore.succeed(`Backup removed`);
          } else {
            spinnerRestore.fail(`Backup left at ${backupPath}`);
          }
        }
      };
      let fNames = fs.readdirSync(getInnerDataDir(dName));
      // only consider csv files
      fNames = fNames.filter((fn) => fn.indexOf(".csv") > -1);
      console.log(
        `Files found in ${dName}: ${JSON.stringify(fNames, null, 4)}`,
      );
      await innerLoop(fNames);
    }
  };
  dirLoop(dirNames).then(() =>
    console.log("Finished processing all files."),
  );
} else if (argv.retry) {
  console.log("Retrying previous errors");
  const outputFiles = path.resolve(__dirname, OUTPUT_DIR);
  const errDirPath = path.resolve(outputFiles, "errors");
  const errDirs = fs.readdirSync(path.resolve(OUTPUT_DIR, "errors"));
  const dirLoops = async (errDirNames: Array<string>) => {
    for (let i = 0; i < errDirNames.length; i++) {
      const errDirName = errDirNames[i];
      const innerErrDir = path.resolve(errDirPath, errDirName);
      const files = fs.readdirSync(innerErrDir);
      // create local backup dir
      const backupDir = path.resolve(BACKUP_DIR, errDirName);
      if (!fs.existsSync(BACKUP_DIR)) {
        fs.mkdirSync(BACKUP_DIR);
      }
      if (!fs.existsSync(backupDir)) {
        fs.mkdirSync(backupDir);
      }
      const fileLoops = async (errFileNames: Array<string>) => {
        for (let i = 0; i < errFileNames.length; i++) {
          const errFileName = errFileNames[i];
          const outputFileName = errFileName.replace("_errors", "");
          const outputFileFullPath = path.resolve(
            outputFiles,
            errDirName,
            outputFileName,
          );
          const errFileFullPath = path.resolve(
            innerErrDir,
            errFileName,
          );

          // Backup file
          if (fs.existsSync(outputFileFullPath)) {
            const backupPath = path.resolve(backupDir, outputFileName);
            const spinnerBackup = new Spinner(
              `Backing up ${outputFileName}`,
            );
            spinnerBackup.start();
            fs.copyFileSync(outputFileFullPath, backupPath);
            spinnerBackup.succeed(
              `${outputFileName} backed up to ${backupPath}`,
            );
          }

          // open output file in append mode
          fs.openSync(outputFileFullPath, "a");
          const TWEET_ID_IDX = 0;
          const SHORT_URL_IDX = 1;
          const retriedFetches: Record<
            string,
            Promise<FetchResponse>
          > = {};
          const urlToTweetIDs: Record<string, Array<string>> = {};
          let fetchOutput: Array<FetchResponse> = [];
          const readPromise: Promise<void> = new Promise((resolve) => {
            const spinnerFetches = new Spinner(
              `Accumulating fetch calls for ${errFileName}`,
            );
            spinnerFetches.start();
            fs.createReadStream(errFileFullPath)
              .pipe(csv.parse({ headers: false }))
              .on("error", (error: Error) => console.error(error))
              .on("data", (row: Array<string>) => {
                const tweetID = row[TWEET_ID_IDX];
                const shortURL = row[SHORT_URL_IDX];
                if (!tweetID || !shortURL) {
                  return;
                }

                if (!urlToTweetIDs[shortURL]) {
                  // Create mapping from URL string to tweet IDs
                  urlToTweetIDs[shortURL] = [tweetID];
                } else {
                  // if mapping already created then we already have a fetch up
                  urlToTweetIDs[shortURL].push(tweetID);
                  return;
                }
                const pFetch = createFetch(shortURL);
                retriedFetches[tweetID] = pFetch;
              })
              .on("end", async (rowCount: number) => {
                const keys = Object.keys(retriedFetches);
                console.log(keys);
                spinnerFetches.succeed(
                  `${keys.length} fetch calls accumulated for ${errFileName} from ${rowCount} rows.`,
                );
                if (keys.length === 0) {
                  console.error(
                    `Could not find any fetches to make for ${errFileName}`,
                  );
                  resolve();
                }

                // wait for HTTP requests to complete
                const spinner = new Spinner(
                  `Expanding URLs for ${errFileName} ...`,
                );
                const promises = keys.map((k) => retriedFetches[k]);
                spinner.start();
                fetchOutput = await Promise.all(promises);
                spinner.succeed("Fetching complete");
                resolve();
              });
          });

          // wait for reads to
          await readPromise;

          // write into files
          // write to file
          const spinnerOutput = new Spinner(
            `Appending retried output rows for ${outputFileName}`,
          );
          spinnerOutput.start();
          const successfulOutput = fetchOutput.filter(
            (row) => row instanceof FetchSuccess,
          );
          const rowsSuccess = createRows(
            successfulOutput,
            urlToTweetIDs,
          );
          const ws = fs.createWriteStream(outputFileFullPath, {
            flags: "a",
          });
          csv
            .writeToStream(ws, rowsSuccess)
            .on("error", (err) => console.error(err))
            .on("finish", () =>
              spinnerOutput.succeed(
                `Done appending to ${outputFileName} with ${rowsSuccess.length} lines.`,
              ),
            );
          const errorOutput = fetchOutput.filter(
            (row) => row instanceof FetchError,
          );
          if (errorOutput.length > 0) {
            const spinnerErrors = new Spinner(
              `Writing error rows for ${errFileName}`,
            );
            spinnerErrors.start();
            const rowsError = createRows(errorOutput, urlToTweetIDs);
            fs.openSync(errFileFullPath, "w");
            csv
              .writeToPath(errFileFullPath, rowsError)
              .on("error", (err) => console.error(err))
              .on("finish", () =>
                spinnerErrors.succeed(
                  `Done rewriting ${errFileName} with ${rowsError.length} lines.`,
                ),
              );
          } else {
            // delete file
            fs.unlinkSync(errFileFullPath);
          }
        }
      };
      await fileLoops(files);
    }
  };
  dirLoops(errDirs).then(() =>
    console.log("Finished processing all error files."),
  );
} else if (argv.combine) {
  /**
   * Reads CSV files from inner directory and concatenates them into a
   * single output file with an extra column indicating the file name
   * @param innerDirName
   */
  async function readAndConcat(innerDirName: string): Promise<void> {
    const innerDirPath = path.resolve(OUTPUT_DIR, innerDirName);
    const newFilePath = path.resolve(OUTPUT_DIR, `${innerDirName}.csv`);
    const ws = fs.createWriteStream(newFilePath, { flags: "w" });
    const innerFiles = fs.readdirSync(innerDirPath);
    const promises = [];
    for (let l = 0; l < innerFiles.length; l++) {
      const fileName = innerFiles[l];
      const innerFilePath = path.resolve(innerDirPath, fileName);
      const readPromise = new Promise<Array<Array<string>>>(
        (resolve, reject) => {
          const rows: Array<Array<string>> = [];
          fs.createReadStream(innerFilePath)
            .pipe(csv.parse({ headers: false }))
            .on("error", (error: Error) => reject(error))
            .on("data", (row: Array<string>) => {
              rows.push(row.concat([fileName.replace(".csv", "")]));
            })
            .on("end", () => {
              resolve(rows);
            });
        },
      );
      promises.push(readPromise);
    }
    const allRows = await Promise.all(promises);
    const concat = allRows.reduce((acc, r) => acc.concat(r), [[]]);
    csv.writeToStream(ws, concat);
  }

  let dirs = fs.readdirSync(OUTPUT_DIR);
  dirs = dirs
    .filter((d) => d.indexOf(".csv") === -1)
    .filter((d) => d !== "errors");
  (async () => {
    for (let k = 0; k < dirs.length; k++) {
      const innerDirName = dirs[k];
      await readAndConcat(innerDirName);
      await del(path.resolve(OUTPUT_DIR, innerDirName));
    }
  })().catch((error) => {
    throw error;
  });
}
